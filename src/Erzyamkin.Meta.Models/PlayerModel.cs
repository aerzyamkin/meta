using System;

namespace Erzyamkin.Meta.Models
{
    public class PlayerModel
    {
        public PlayerModel(int playerId, DateTime created, DateTime lastLogin, DateTime lastLogout, int sessionCount, bool entered)
        {
            PlayerId = playerId;
            Created = created;
            LastLogin = lastLogin;
            LastLogout = lastLogout;
            SessionCount = sessionCount;
            Entered = entered;
        }

        public int PlayerId { get; }

        public DateTime Created { get; }

        public DateTime LastLogin { get; }

        public DateTime LastLogout { get; }

        public int SessionCount { get; }
        
        public bool Entered { get; }
    }
}