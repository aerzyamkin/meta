using System.IO;

namespace Erzyamkin.Common
{
    public static class AkkaConfigReader
    {
        public static Akka.Configuration.Config GetAkkaConfig(string configFile)
        {
            string akkaConfigStr = File.ReadAllText(configFile);
            return Akka.Configuration.ConfigurationFactory.ParseString(akkaConfigStr);
        }
    }
}