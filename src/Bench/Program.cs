﻿using System;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

namespace Bench
{
    class Program
    {
        static void Main(string[] args)
        {
            var summary1 = BenchmarkRunner.Run<RandomBench>();
            var summary2 = BenchmarkRunner.Run<ThreadSafeRandomBench>();
        }
    }

    [CoreJob, MemoryDiagnoser]
    public class RandomBench
    {
        [Benchmark]
        public long Do()
        {
            var sum = 0L;
            for (int iteration = 0; iteration < 10000; ++iteration)
            {
                var random = new Random(iteration);

                sum += random.Next(100, 10000);
                sum += random.Next(100, 10000);
                sum += random.Next(100, 10000);
                sum += random.Next(100, 10000);
                sum += random.Next(100, 10000);
            }

            return sum;
        }
    }

    [CoreJob, MemoryDiagnoser]
    public class ThreadSafeRandomBench
    {
        private readonly IRandom _random = new ThreadSafeRandom();

        [Benchmark]
        public long Do()
        {
            var sum = 0L;
            for (int iteration = 0; iteration < 10000; ++iteration)
            {
                sum += _random.Next(100, 10000);
                sum += _random.Next(100, 10000);
                sum += _random.Next(100, 10000);
                sum += _random.Next(100, 10000);
                sum += _random.Next(100, 10000);
            }

            return sum;
        }
    }

    public interface IRandom
    {
        int Next(int minValue, int maxValue);
    }

    public class ThreadSafeRandom : IRandom
    {
        private readonly object _lock = new object();
        private readonly Random _random;

        public ThreadSafeRandom()
        {
            _random = new Random();
        }

        public ThreadSafeRandom(int seed)
        {
            _random = new Random(seed);
        }

        public int Next(int minValue, int maxValue)
        {
            lock (_lock)
            {
                return _random.Next(minValue, maxValue);
            }
        }
    }
}
