﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erzyamkin.Mafia.Domain
{
    public class Game
    {
        private readonly List<Player> _players = new List<Player>();

        public Game(IEnumerable<Player> players)
        {
            _players.AddRange(players);
        }

        public void MakeTurn()
        {

        }
    }

    public abstract class GameState
    {
        public abstract GameState MakeTurn();
    }
}
