﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Erzyamkin.Mafia.Domain
{
    static class MAssert
    {
        public static void NotNull<T>(T value)
            where T : class
        {
            Debug.Assert(value != null);
        }

        public static void NotNullOrEmpty<TValue>(IEnumerable<TValue> value)
            //where TCollection : class, IEnumerable<TValue>
        {
            NotNull(value);

            if (value is ICollection collection)
            {
                Debug.Assert(collection.Count > 0);
            }
            else
            {
                Debug.Assert(value.Any());
            }
        }
    }
}
