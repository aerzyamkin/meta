﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Erzyamkin.Mafia.Domain
{
    public class GameBuilder
    {
        private readonly List<PlayerBuildParams> _players = new List<PlayerBuildParams>();

        public GameBuilder AddPlayer(PlayerBuildParams playerBuildParams)
        {
            _players.Add(playerBuildParams);

            return this;
        }

        public Game Build()
        {
            var gamePlayers = _players.Select(BuildPlayer);

            return new Game(gamePlayers);
        }

        private Player BuildPlayer(PlayerBuildParams playerBuildParams)
        {
            return new Player(playerBuildParams.Role);
        }
    }
}
