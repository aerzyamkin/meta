﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erzyamkin.Mafia.Domain
{
    public class Player
    {
        public Player(PlayerRole role)
        {
            Role = role;
        }

        public PlayerRole Role { get; }
    }
}
