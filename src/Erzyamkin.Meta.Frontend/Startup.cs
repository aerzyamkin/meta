﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using Akka.Actor;
using Akka.Routing;
using Erzyamkin.Meta.Frontend.Actors;

namespace Erzyamkin.Meta.Frontend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var akka = CreateAkka();
            
            services
                .AddSingleton(akka)
                .AddSingleton<Services.IPlayersService, Services.PlayersService>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseMvc();
        }

        private SystemActors CreateAkka()
        {
            var actorSystemName = Configuration["Akka:ActorSystemName"];
            string akkaConfigFile = Configuration["Akka:ConfigFile"];
            
            var akkaConfig = Common.AkkaConfigReader.GetAkkaConfig(akkaConfigFile);

            var actorSystem = ActorSystem.Create(actorSystemName, akkaConfig);
            var mediator = actorSystem.ActorOf(Props.Empty.WithRouter(FromConfig.Instance), "players_mediator");

            return new SystemActors(actorSystem, mediator);
        }
    }
}
