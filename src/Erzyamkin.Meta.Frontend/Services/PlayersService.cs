using System;
using System.Threading.Tasks;
using System.Linq;

using Akka.Actor;
using Akka.Cluster;
using Akka.Routing;
using Erzyamkin.Meta.Frontend.Actors;
using Erzyamkin.Meta.Messages;
using Erzyamkin.Meta.Models;

namespace Erzyamkin.Meta.Frontend.Services
{
    class PlayersService : IPlayersService
    {
        private readonly SystemActors _systemActors;
        private readonly Cluster _cluster;

        public PlayersService(SystemActors systemActors)
        {
            _systemActors = systemActors;
            _cluster = Cluster.Get(_systemActors.System);
        }

        public async Task<PlayerModel> Enter(int playerId)
        {
            var result = await _systemActors.PlayersMediator.Ask<PlayerModel>(Envelope(new PlayersSupervisor.PlayerEnterCommand(playerId)));
            
            return result;
        }

        public Task Leave(int playerId)
        {
            _systemActors.PlayersMediator.Tell(Envelope(new PlayersSupervisor.PlayerLeaveCommand(playerId)));
            
            return Task.CompletedTask;
        }
        
        public Task<Models.PlayerModel> State(int playerId)
        {
            return _systemActors.PlayersMediator.Ask<PlayerModel>(Envelope(new GetPlayerStateQuery(playerId)));
        }

        private ConsistentHashableEnvelope Envelope<T>(T request)
            where T : class, IRequestWithPlayerId
        {
            return new ConsistentHashableEnvelope(request, request.PlayerId);
        }
    }
}