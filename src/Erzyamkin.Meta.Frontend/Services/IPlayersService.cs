using System.Threading.Tasks;

namespace Erzyamkin.Meta.Frontend.Services
{
    public interface IPlayersService
    {
        Task<Models.PlayerModel> Enter(int playerId);
        
        Task Leave(int playerId);
        
        Task<Models.PlayerModel> State(int playerId);
    }
}
