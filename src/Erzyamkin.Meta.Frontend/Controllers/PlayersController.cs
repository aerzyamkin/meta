using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

namespace Erzyamkin.Meta.Frontend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayersController : ControllerBase
    {
        private readonly Services.IPlayersService _players;

        public PlayersController(Services.IPlayersService players)
        {
            _players = players;
        }

        [HttpPost("enter")]
        public async Task<ActionResult<Models.PlayerModel>> Enter([FromBody] string idStr)
        {
            var playerId = int.Parse(idStr);
            
            var playerModel = await _players.Enter(playerId);
            
            return Ok(playerModel);
        }

        [HttpPost("leave")]
        public async Task<ActionResult> Leave([FromBody] string idStr)
        {
            var playerId = int.Parse(idStr);
            
            await _players.Leave(playerId);

            return Ok();
        }
        
        [HttpPost("state")]
        public async Task<ActionResult<Models.PlayerModel>> State([FromBody] string idStr)
        {
            var playerId = int.Parse(idStr);
            
            var playerModel = await _players.State(playerId);
            
            return Ok(playerModel);
        }
    }
}