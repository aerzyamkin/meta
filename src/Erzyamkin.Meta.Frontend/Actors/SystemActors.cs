using Akka.Actor;

namespace Erzyamkin.Meta.Frontend.Actors
{
    public class SystemActors
    {
        public SystemActors(ActorSystem system, IActorRef playersMediator)
        {
            System = system;
            PlayersMediator = playersMediator;
        }

        public ActorSystem System { get; }
        
        public IActorRef PlayersMediator { get; }
    }
}