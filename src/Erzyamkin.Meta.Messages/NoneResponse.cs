namespace Erzyamkin.Meta
{
    public sealed class NoneResponse
    {
        public static NoneResponse Instance { get; } = new NoneResponse();
    }
}