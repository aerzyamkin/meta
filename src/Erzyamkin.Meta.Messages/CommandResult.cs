using System;

namespace Erzyamkin.Meta
{
    public class CommandResult<TValue, TErrorCodes>
        where TErrorCodes : struct, Enum
    {
        protected CommandResult(TValue value)
        {
            Value = value;
        }

        protected CommandResult(TErrorCodes errorCode, string errorMessage)
        {
            ErrorCode = errorCode;
            ErrorMessage = errorMessage;
        }
        
        public TValue Value { get; }
        public TErrorCodes? ErrorCode { get; }
        public string ErrorMessage { get; }

        public bool IsFailed
            => ErrorCode.HasValue;
        
        public bool IsSucceeded
            => !IsFailed;
        
        public static CommandResult<TValue, TErrorCodes> Success(TValue value)
            => new CommandResult<TValue, TErrorCodes>(value);

        public static CommandResult<TValue, TErrorCodes> Fail(TErrorCodes errorCode, string errorMessage = null)
            => new CommandResult<TValue, TErrorCodes>(errorCode, errorMessage);

        public static implicit operator bool(CommandResult<TValue, TErrorCodes> commandResult)
            => commandResult.IsSucceeded;
    }

    public enum CommandResultErrorCodes
    {
        Error
    }

    public sealed class CommandResult<TValue> : CommandResult<TValue, CommandResultErrorCodes>
    {
        private CommandResult(TValue value)
            : base(value)
        {
        }

        private CommandResult(CommandResultErrorCodes errorCode, string errorMessage)
            : base(errorCode, errorMessage)
        {
        }

        public new static CommandResult<TValue> Success(TValue value)
            => new CommandResult<TValue>(value);

        public static CommandResult<TValue> Fail(string errorMessage = null)
            => new CommandResult<TValue>(CommandResultErrorCodes.Error, errorMessage);
    }
}