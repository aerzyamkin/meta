namespace Erzyamkin.Meta.Messages
{
    public static partial class PlayersSupervisor
    {
        public sealed class PlayerEnterCommand : IRequest, IRequestWithPlayerId
        {
            public PlayerEnterCommand(int playerId)
            {
                PlayerId = playerId;
            }

            public int PlayerId { get; }
        }
    }
}