namespace Erzyamkin.Meta.Messages
{
    public static partial class PlayersSupervisor
    {
        public sealed class PlayerLeaveCommand : IRequest, IRequestWithPlayerId
        {
            public PlayerLeaveCommand(int playerId)
            {
                PlayerId = playerId;
            }

            public int PlayerId { get; }
        }
    }
}