namespace Erzyamkin.Meta
{
    public interface IRequest<out TResult> {}
    
    public interface IRequest : IRequest<NoneResponse> {}
}