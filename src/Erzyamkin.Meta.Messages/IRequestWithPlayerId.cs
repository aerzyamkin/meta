namespace Erzyamkin.Meta.Messages
{
    public interface IRequestWithPlayerId
    {
        int PlayerId { get; }
    }
}