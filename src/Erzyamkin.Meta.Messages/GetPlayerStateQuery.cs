namespace Erzyamkin.Meta.Messages
{
    public sealed class GetPlayerStateQuery : IRequest, IRequestWithPlayerId
    {
        public GetPlayerStateQuery(int playerId)
        {
            PlayerId = playerId;
        }

        public int PlayerId { get; }
    }
}