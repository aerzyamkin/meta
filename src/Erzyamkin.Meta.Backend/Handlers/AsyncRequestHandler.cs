using System.Threading.Tasks;

namespace Erzyamkin.Meta
{
    public abstract class AsyncRequestHandler<TRequest> :
        IAsyncRequestHandler<TRequest, NoneResponse>
        where TRequest : IRequest<NoneResponse>
    {
        async Task<NoneResponse> IAsyncRequestHandler<TRequest, NoneResponse>.Handle(TRequest command)
        {
            await Handle(command).ConfigureAwait(false);
            
            return NoneResponse.Instance;
        }

        protected abstract Task Handle(TRequest command);
    }
}