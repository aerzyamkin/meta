namespace Erzyamkin.Meta
{
    public abstract class RequestHandler<TRequest> :
        IRequestHandler<TRequest, NoneResponse>
        where TRequest : IRequest<NoneResponse>
    {
        NoneResponse IRequestHandler<TRequest, NoneResponse>.Handle(TRequest command)
        {
            Handle(command);
            
            return NoneResponse.Instance;
        }

        protected abstract void Handle(TRequest command);
    }
}