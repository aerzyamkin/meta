﻿using System.Threading.Tasks;

namespace Erzyamkin.Meta.Backend
{
    delegate Task<TResponse> HandlerDelegate<TResponse>();

    interface IHandlerMiddleware<in TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        Task<TResponse> Handle(TRequest request, HandlerDelegate<TResponse> next);
    }
}
