using System.Threading.Tasks;

namespace Erzyamkin.Meta
{
    public interface IAsyncRequestHandler<in TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        Task<TResponse> Handle(TRequest command);
    }
}