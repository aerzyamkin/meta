﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Erzyamkin.Meta.Backend.Handlers.Builders
{
    class HandlerBuilder : IHandlerBuilder
    {
        public IHandler<TRequest, TResponse> Add<TRequest, TResponse>(IAsyncRequestHandler<TRequest, TResponse> handler)
            where TRequest : IRequest<TResponse>
        {
            return new Handler<TRequest, TResponse>(handler);
        }
    }

    class Handler<TRequest, TResponse> : IHandler<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        private readonly List<Type> _middlewareTypes = new List<Type>();

        private readonly IAsyncRequestHandler<TRequest, TResponse> _handler;
        
        public Handler(IAsyncRequestHandler<TRequest, TResponse> handler)
        {
            _handler = handler;
        }

        public IHandler<TRequest, TResponse> Add<TMiddleware>()
            where TMiddleware : IHandlerMiddleware<TRequest, TResponse>
        {
            Type middlewareType = typeof(TMiddleware).MakeGenericType(typeof(TRequest), typeof(TResponse));

            _middlewareTypes.Add(middlewareType);

            return this;
        }

        public Func<TRequest, Task<TResponse>> BuildAsync()
        {
            Func<TRequest, Task<TResponse>> result = async req =>
            {
                var mws = new List<IHandlerMiddleware<TRequest, TResponse>>(_middlewareTypes.Count);
                foreach (Type eachMwType in _middlewareTypes)
                {
                    var middleware = (IHandlerMiddleware<TRequest, TResponse>)Activator.CreateInstance(eachMwType);
                    mws.Add(middleware);
                }
                
                return await Run(0, mws, req);
            };

            return result;
            
            async Task<TResponse> Run(int index, List<IHandlerMiddleware<TRequest, TResponse>> mws, TRequest request)
            {
                if (index >= mws.Count)
                {
                    var response = await _handler.Handle(request);
                    return response;
                }
                
                return await mws[index].Handle(request, () => Run(index + 1, mws, request));
            }
        }
    }
}
