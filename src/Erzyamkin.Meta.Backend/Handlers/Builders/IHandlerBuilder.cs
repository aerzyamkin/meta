﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Erzyamkin.Meta.Backend.Handlers.Builders
{
    interface IHandler<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        IHandler<TRequest, TResponse> Add<TMiddleware>()
            where TMiddleware : IHandlerMiddleware<TRequest, TResponse>;

        //Func<TRequest, TResponse> Build();
        Func<TRequest, Task<TResponse>> BuildAsync();
    }

    interface IHandlerBuilder
    {
        IHandler<TRequest, TResponse> Add<TRequest, TResponse>(IAsyncRequestHandler<TRequest, TResponse> handler)
            where TRequest : IRequest<TResponse>;
    }
}
