﻿using System;
using System.IO;
using Akka.Actor;
using Microsoft.Extensions.Configuration;

namespace Erzyamkin.Meta.Backend
{
    class Program
    {
        private IConfiguration Configuration { get; }

        static void Main(string[] args)
        {
            var program = new Program();
            program.Start();
        }

        private Program()
        {
            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Configuration = configBuilder.Build();
        }

        private void Start()
        {
            string dataPath = Configuration["Players:DataPath"];
            string fullPath = Path.GetFullPath(dataPath);

            var playersStorage = new FilePlayersStorage(fullPath);

            using (var system = CreateAkka())
            {
                var supervisor = system.ActorOf(Actors.PlayersSupervisorActor.Props(playersStorage), "players");

                Console.WriteLine("Press ENTER to exit...");

                Console.ReadLine();
            }
        }

        private ActorSystem CreateAkka()
        {
            var actorSystemName = Configuration["Akka:ActorSystemName"];
            string akkaConfigFile = Configuration["Akka:ConfigFile"];
            
            var akkaConfig = Common.AkkaConfigReader.GetAkkaConfig(akkaConfigFile);

            return ActorSystem.Create(actorSystemName, akkaConfig);
        }
    }
}
