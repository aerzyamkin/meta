﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Erzyamkin.Meta.Backend
{
    class FilePlayersStorage : IPlayersStorage
    {
        private readonly string _fullPath;

        public FilePlayersStorage(string fullPath)
        {
            _fullPath = fullPath;
        }

        public async Task<Actors.PlayerState> Load(int playerId)
        {
            string fullFileName = Path.Combine(_fullPath, $"{playerId}.json");

            Actors.PlayerState state;
            if (File.Exists(fullFileName))
            {
                string content;
                using (var streamReader = new StreamReader(fullFileName, Encoding.UTF8))
                {
                    content = await streamReader.ReadToEndAsync();
                }

                state = Jil.JSON.Deserialize<Actors.PlayerState>(content);

                //Log.Info("Player {0} loaded", playerId);
            }
            else
            {
                state = Actors.PlayerState.None;

                //Log.Info("Player {0} not found", msg.PlayerId);
            }

            return state;
        }

        public async Task Save(Actors.PlayerState playerState)
        {
            if (!Directory.Exists(_fullPath))
            {
                Directory.CreateDirectory(_fullPath);
            }

            string fullFileName = Path.Combine(_fullPath, $"{playerState.PlayerId}.json");
            string fullTempFileName = Path.Combine(_fullPath, $"{playerState.PlayerId}.1.json");

            string content = Jil.JSON.Serialize(playerState);

            using (var streamWriter = new StreamWriter(fullTempFileName, false, Encoding.UTF8))
            {
                await streamWriter.WriteAsync(content);
                await streamWriter.FlushAsync();
            }

            File.Delete(fullFileName);
            File.Move(fullTempFileName, fullFileName);

            //_log.Info("Player {0} saved", command.PlayerState.PlayerId);
        }
    }
}
