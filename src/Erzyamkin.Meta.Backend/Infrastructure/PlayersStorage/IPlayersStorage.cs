﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Erzyamkin.Meta.Backend
{
    interface IPlayersStorage
    {
        Task Save(Actors.PlayerState playerState);

        Task<Actors.PlayerState> Load(int playerId);
    }
}
