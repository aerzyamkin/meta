using System;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.Event;

namespace Erzyamkin.Meta.Backend.Actors
{
    abstract class HandlerActor : ReceiveActor, ILogReceive
    {
        protected ILoggingAdapter Log { get; } = Context.GetLogger();
        
        protected abstract string ActorInfo { get; }

        protected override void PreStart()
        {
            Log.Info($"{GetType().Name} {ActorInfo} starting");
        }

        protected override void PostStop() => Log.Info($"{GetType().Name} {ActorInfo} stopped");
        
        protected virtual void Handle<TRequest, TResponse>(IRequestHandler<TRequest, TResponse> handler)
            where TRequest : IRequest<TResponse>
        {
            Receive<TRequest>(msg =>
            {
                var response = handler.Handle(msg);

                if (IsNotNone(response))
                {
                    Sender.Tell(response, Self);
                }
            });
        }

        protected virtual void Handle<TRequest, TResponse>(IAsyncRequestHandler<TRequest, TResponse> handler, bool wait = true)
            where TRequest : IRequest<TResponse>
        {
            if (wait)
            {
                ReceiveAsync<TRequest>(async request =>
                {
                    var response = await handler.Handle(request);

                    if (IsNotNone(response))
                    {
                        Sender.Tell(response, Self);
                    }
                });
            }
            else
            {
                Receive<TRequest>(request =>
                {
                    var sender = Sender;
                    handler.Handle(request).ContinueWith(
                        task =>
                        {
                            var response = task.Result;
                            if (IsNotNone(response))
                            {
                                task.PipeTo(sender);
                            }
                        },
                        TaskContinuationOptions.ExecuteSynchronously);
                });
            }
        }

        private static bool IsNotNone<TResponse>(TResponse result)
        {
            return !(result is NoneResponse);
        }
    }
}