using System.Threading.Tasks;
using Akka.Actor;

namespace Erzyamkin.Meta.Backend.Actors
{
    abstract class HandlerActor<TState> : HandlerActor, IWithUnboundedStash
        where TState : class, new()
    {
        public IStash Stash { get; set; }

        protected TState State { get; private set; }

        protected override void PreStart()
        {
            base.PreStart();

            Become(Starting);

            Self.Tell(Commands.Start.Instance);
        }
        
        protected abstract Task SaveState();
        protected abstract Task<TState> LoadState();

        protected virtual Task<TState> CreateState()
        {
            return Task.FromResult(new TState());
        }

        protected override void Handle<TRequest, TResponse>(IRequestHandler<TRequest, TResponse> handler)
        {
            var handlerWrapper = SaveStateHandlerWrapper<TRequest, TResponse>.Create(this, handler);
            
            base.Handle(handlerWrapper);
        }

        protected override void Handle<TRequest, TResponse>(IAsyncRequestHandler<TRequest, TResponse> handler, bool wait = true)
        {
            var handlerWrapper = SaveStateHandlerWrapper<TRequest, TResponse>.CreateAsync(this, handler);
            
            base.Handle(handlerWrapper, wait);
        }
        
        protected virtual Task OnStarted()
        {
            Log.Info($"{GetType().Name} {ActorInfo} started");

            return Task.CompletedTask;
        }

        private void Starting()
        {
            ReceiveAsync<Commands.Start>(msg => StartAction());
            ReceiveAny(_ => Stash.Stash());
        }

        private async Task StartAction()
        {
            State = await LoadState();
            if (State == null)
            {
                State = new TState();

                //await SaveState();
            }

            //Become(Started);
            await OnStarted();

            Stash.UnstashAll();
        }

        static class Commands
        {
            public sealed class Start
            {
                public static Start Instance { get; } = new Start();
            }
        }

        class SaveStateHandlerWrapper<TCommand, TResult> : IAsyncRequestHandler<TCommand, TResult>
            where TCommand : IRequest<TResult>
        {
            private readonly HandlerActor<TState> _actor;
            
            private readonly IRequestHandler<TCommand, TResult> _handler;
            private readonly IAsyncRequestHandler<TCommand, TResult> _asyncHandler;

            private SaveStateHandlerWrapper(HandlerActor<TState> actor, IRequestHandler<TCommand, TResult> handler)
            {
                _actor = actor;
                _handler = handler;
            }

            private SaveStateHandlerWrapper(HandlerActor<TState> actor, IAsyncRequestHandler<TCommand, TResult> asyncHandler)
            {
                _actor = actor;
                _asyncHandler = asyncHandler;
            }

            public static SaveStateHandlerWrapper<TCommand, TResult> Create(HandlerActor<TState> actor, IRequestHandler<TCommand, TResult> handler)
            {
                return new SaveStateHandlerWrapper<TCommand, TResult>(actor, handler);
            }
            
            public static SaveStateHandlerWrapper<TCommand, TResult> CreateAsync(HandlerActor<TState> actor, IAsyncRequestHandler<TCommand, TResult> handler)
            {
                return new SaveStateHandlerWrapper<TCommand, TResult>(actor, handler);
            }

            public async Task<TResult> Handle(TCommand command)
            {
                TResult result;
                if (_handler != null)
                {
                    result = _handler.Handle(command);
                }
                else
                {
                    result = await _asyncHandler.Handle(command);
                }

                await _actor.SaveState();
                
                return result;
            }
        }
    }
}