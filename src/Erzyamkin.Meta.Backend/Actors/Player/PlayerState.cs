using System;

namespace Erzyamkin.Meta.Backend.Actors
{
    class PlayerState
    {
        public static PlayerState None { get; } = new PlayerState { PlayerId = -1 };

        public PlayerState()
        {
        }

        public PlayerState(PlayerState other)
        {
            PlayerId = other.PlayerId;
            Created = other.Created;
            LastLogin = other.LastLogin;
            LastLogout = other.LastLogout;
            SessionCount = other.SessionCount;
        }

        public PlayerState Clone()
        {
            return new PlayerState(this);
        }

        public int PlayerId { get; set; }

        public DateTime Created { get; set; }

        public DateTime LastLogin { get; set; }

        public DateTime LastLogout { get; set; }

        public int SessionCount { get; set; }
    }
}
