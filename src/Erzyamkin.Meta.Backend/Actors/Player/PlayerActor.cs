using System;
using System.Threading.Tasks;

using Akka.Actor;
// using Akka.Logger.Serilog;

namespace Erzyamkin.Meta.Backend.Actors
{
    partial class PlayerActor : HandlerActor<PlayerState>
    {        
        private static readonly TimeSpan IdleTimeout = TimeSpan.FromSeconds(10);

        private readonly int _playerId;
        private readonly IPlayersStorage _storage;
        
        private bool _entered;

        public PlayerActor(int playerId, IPlayersStorage storage)
        {
            _playerId = playerId;
            _storage = storage;
            
            Context.SetReceiveTimeout(IdleTimeout);
        }

        public static Props Props(int playerId, IPlayersStorage storage)
            => Akka.Actor.Props.Create(() => new PlayerActor(playerId, storage));

        public static string Name(long playerId)
            => $"player-{playerId}";
        
        protected override string ActorInfo
            => $"{nameof(PlayerActor)} {_playerId}";

        protected override Task OnStarted()
        {
            Become(() =>
            {
                Handle(new EnterCommandHandler(this));
                Handle(new LeaveCommandHandler(this));
                Handle(new GetStateQueryHandler(this));
                
                Receive<ReceiveTimeout>(msg =>
                {
                    if (Context is ActorCell cell && cell.HasMessages)
                    {
                        Log.Debug("Can't stop actor when mailbox has messages");
                        
                        return;
                    }
                    
                    Context.Stop(Self);
                });
            });

            return Task.CompletedTask;
        }

        protected override Task SaveState()
            => _storage.Save(State);

        protected override Task<PlayerState> LoadState()
            => _storage.Load(_playerId);
        
        private Models.PlayerModel ToModel()
        {
            return new Models.PlayerModel(
                State.PlayerId,
                State.Created,
                State.LastLogin,
                State.LastLogout,
                State.SessionCount,
                _entered
            );
        }

        //protected override Task<PlayerState> CreateState()
        //    => Task.FromResult(new PlayerState
        //    {
        //        PlayerId = _playerId,
        //        Created = DateTime.UtcNow
        //    });
    }
}