//namespace Erzyamkin.Meta.Backend.Actors
//{
//    partial class Player
//    {
//        class GetStateQueryHandler : IRequestHandler<Queries.GetState, Models.PlayerModel>
//        {
//            private readonly Player _player;

//            public GetStateQueryHandler(Player player)
//            {
//                _player = player;
//            }

//            public Models.PlayerModel Handle(Queries.GetState query)
//            {
//                return new Models.PlayerModel(
//                    _player.State.PlayerId,
//                    _player.State.Created,
//                    _player.State.LastLogin,
//                    _player.State.LastLogout,
//                    _player.State.SessionCount
//                );
//            }
//        }
//    }
//}