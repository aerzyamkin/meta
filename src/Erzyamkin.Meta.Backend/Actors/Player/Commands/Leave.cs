﻿using System;

namespace Erzyamkin.Meta.Backend.Actors
{
    partial class PlayerActor
    {
        public static partial class Commands
        {
            public sealed class Leave : IRequest
            {
                public static Leave Instance { get; } = new Leave();
            }
        }

        class LeaveCommandHandler : RequestHandler<Commands.Leave>
        {
            private readonly PlayerActor _player;

            public LeaveCommandHandler(PlayerActor player)
            {
                _player = player;
            }

            protected override void Handle(Commands.Leave command)
            {
                if (!_player._entered)
                {
                    return;
                }
                
                Context.SetReceiveTimeout(IdleTimeout);

                _player.State.LastLogout = DateTime.UtcNow;
            }
        }
    }
}
