﻿using System;

namespace Erzyamkin.Meta.Backend.Actors
{
    partial class PlayerActor
    {
        public static partial class Commands
        {
            public sealed class Enter : IRequest<Models.PlayerModel>
            {
                public static Enter Instance { get; } = new Enter();
            }
        }

        class EnterCommandHandler : IRequestHandler<Commands.Enter, Models.PlayerModel>
        {
            private readonly PlayerActor _player;

            public EnterCommandHandler(PlayerActor player)
            {
                _player = player;
            }

            public Models.PlayerModel Handle(Commands.Enter command)
            {
                if (_player._entered)
                {
                    return null;
                }
                
                Context.SetReceiveTimeout(null);

                if (_player.State.Equals(PlayerState.None))
                {
                    _player.State.PlayerId = _player._playerId;
                    _player.State.Created = DateTime.UtcNow;
                }

                _player.State.LastLogin = DateTime.UtcNow;
                _player.State.SessionCount += 1;

                _player._entered = true;

                return _player.ToModel();
            }
        }
    }
}
