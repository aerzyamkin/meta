﻿namespace Erzyamkin.Meta.Backend.Actors
{
    partial class PlayerActor
    {
        public static partial class Queries
        {
            public sealed class GetState: IRequest<Models.PlayerModel>
            {
                public static GetState Instance { get; } = new GetState();
            }
        }

        class GetStateQueryHandler : IRequestHandler<Queries.GetState, Models.PlayerModel>
        {
            private readonly PlayerActor _player;

            public GetStateQueryHandler(PlayerActor player)
            {
                _player = player;
            }

            public Models.PlayerModel Handle(Queries.GetState query)
            {
                return _player.ToModel();
            }
        }
    }
}
