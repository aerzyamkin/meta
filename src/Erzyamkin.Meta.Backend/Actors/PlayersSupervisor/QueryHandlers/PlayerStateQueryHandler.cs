using System.Threading.Tasks;
using Akka.Actor;
using Erzyamkin.Meta.Backend.Actors;

namespace Erzyamkin.Meta.Backend.Actors
{
    partial class PlayersSupervisorActor
    {
        class PlayerStateQueryHandler : RequestHandler<Messages.GetPlayerStateQuery>
        {
            private readonly PlayersSupervisorActor _actor;
            
            public PlayerStateQueryHandler(PlayersSupervisorActor actor)
            {
                _actor = actor;
            }

            protected override void Handle(Messages.GetPlayerStateQuery query)
            {
                var playerActorRef = _actor.GetOrCreatePlayerActor(query.PlayerId);
                
                playerActorRef.Tell(PlayerActor.Queries.GetState.Instance, Context.Sender);
            }
        }
    }
}