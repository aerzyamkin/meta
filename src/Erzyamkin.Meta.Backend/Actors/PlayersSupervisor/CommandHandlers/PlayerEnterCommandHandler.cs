using Akka.Actor;

namespace Erzyamkin.Meta.Backend.Actors
{
    partial class PlayersSupervisorActor
    {
        class PlayerEnterCommandHandler : RequestHandler<Messages.PlayersSupervisor.PlayerEnterCommand>
        {
            private readonly PlayersSupervisorActor _actor;

            public PlayerEnterCommandHandler(PlayersSupervisorActor actor)
            {
                _actor = actor;
            }

            protected override void Handle(Messages.PlayersSupervisor.PlayerEnterCommand message)
            {
                var playerActorRef = _actor.GetOrCreatePlayerActor(message.PlayerId);
                
                playerActorRef.Tell(PlayerActor.Commands.Enter.Instance, Context.Sender);
            }
        }
    }
}