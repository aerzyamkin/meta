using Akka.Actor;

namespace Erzyamkin.Meta.Backend.Actors
{
    partial class PlayersSupervisorActor
    {
        class PlayerLeaveCommandHandler : RequestHandler<Messages.PlayersSupervisor.PlayerLeaveCommand>
        {
            private readonly PlayersSupervisorActor _actor;
            
            public PlayerLeaveCommandHandler(PlayersSupervisorActor actor)
            {
                _actor = actor;
            }

            protected override void Handle(Messages.PlayersSupervisor.PlayerLeaveCommand command)
            {
                var playerActorRef = _actor.GetPlayerActor(command.PlayerId);
                if (!playerActorRef.Equals(Nobody.Instance))
                {
                    playerActorRef.Tell(PlayerActor.Commands.Leave.Instance);
                }
            }
        }
    }
}