using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.Routing;

namespace Erzyamkin.Meta.Backend.Actors
{
    partial class PlayersSupervisorActor : HandlerActor
    {
        private readonly Dictionary<int, IActorRef> _players = new Dictionary<int, IActorRef>();
        private readonly Dictionary<IActorRef, int> _playersReversed = new Dictionary<IActorRef, int>();
        
        private readonly IPlayersStorage _storage;

        public PlayersSupervisorActor(IPlayersStorage storage)
        {
            _storage = storage;

            Handle(new PlayerEnterCommandHandler(this));
            Handle(new PlayerLeaveCommandHandler(this));
            Handle(new PlayerStateQueryHandler(this));
            
            Receive<Terminated>(msg =>
            {
                if (_playersReversed.TryGetValue(msg.ActorRef, out int playerId))
                {
                    _players.Remove(playerId);
                    _playersReversed.Remove(msg.ActorRef);
                    
                    Log.Info("Terminated player removed. Id = {0}", playerId.ToString());
                }
                else
                {
                    Log.Warning("Terminated player not found");
                }

                Context.Unwatch(msg.ActorRef);
            });
        }

        public static Props Props(IPlayersStorage storage)
            => Akka.Actor.Props.Create(() => new PlayersSupervisorActor(storage));

        protected override string ActorInfo
            => nameof(PlayersSupervisorActor);

        private IActorRef GetPlayerActor(int playerId)
        {
            if (_players.TryGetValue(playerId, out var player))
            {
                return player;
            }
            
            return Nobody.Instance;
        }

        private IActorRef CreatePlayerActor(int playerId)
        {
            var playerProps = PlayerActor.Props(playerId, _storage)
                .WithSupervisorStrategy(new OneForOneStrategy(ex => Directive.Restart));

            var playerName = PlayerActor.Name(playerId);
            var player = Context.ActorOf(playerProps, playerName);

            Context.Watch(player);

            _players[playerId] = player;
            _playersReversed[player] = playerId;

            return player;
        }
        
        private IActorRef GetOrCreatePlayerActor(int playerId)
        {
            var player = GetPlayerActor(playerId);
            if (player.Equals(Nobody.Instance))
            {
                player = CreatePlayerActor(playerId);
            }

            return player;
        }
    }
}