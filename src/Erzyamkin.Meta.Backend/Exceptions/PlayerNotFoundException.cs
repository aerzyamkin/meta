using System;

namespace Erzyamkin.Meta.Backend
{
    public class PlayerNotFoundException : Exception
    {
        public PlayerNotFoundException(int playerId)
        {
            PlayerId = playerId;
        }

        public int PlayerId { get; }
    }
}