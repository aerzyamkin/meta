﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using Akka.Actor;
using Akka.Event;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Erzyamkin.Seed
{
    class Program
    {
        private IConfiguration Configuration { get; }

        static void Main(string[] args)
        {
            var program = new Program();
            program.Start();
        }

        private Program()
        {
            var configBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Configuration = configBuilder.Build();
        }

        private void Start()
        {
            using (var system = CreateAkka())
            {
                Console.WriteLine("Press ENTER to exit...");

                Console.ReadLine();
            }
        }

        private ActorSystem CreateAkka()
        {
            var actorSystemName = Configuration["Akka:ActorSystemName"];
            string akkaConfigFile = Configuration["Akka:ConfigFile"];
            
            var akkaConfig = Common.AkkaConfigReader.GetAkkaConfig(akkaConfigFile);

            return ActorSystem.Create(actorSystemName, akkaConfig);
        }
    }
}
